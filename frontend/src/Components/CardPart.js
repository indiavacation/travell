import React, { useEffect, useState } from 'react'
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import axios from 'axios';
import { Card,} from 'reactstrap';




function CardPart() {


 
  let [data, setData] = useState(null)
  useEffect(() => {
    const fetchData = async () => {
      try {
        let response = await axios.get("http://localhost:3001/home");
        console.log(response.data);
        setData(response.data)
      }
      catch (err) {
        console.log(err);
      }

    }
    fetchData();
  }, [])

  
  return (
    <div>
      

<div className='card-container'>
{data && data.map((item)=>(
<Card
style={{
 
 boxSizing:'border-box',
  margin:'15px',
  height:'30%',
  width:'30%',
  

}}
> 

<img
  alt="Card cap"
  src={item.imgurl}
  width="100%"
  
 
 
/>

</Card>

))}
</div>



</div>
  )
}

export default CardPart