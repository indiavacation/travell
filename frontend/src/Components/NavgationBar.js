import React from 'react'
import {Link} from 'react-router-dom'


function NavigationBar() {
  return (
    <div>
       <nav className="navbar navbar-expand-lg navbar-light bg-light">
      
 
  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span className="navbar-toggler-icon"></span>
  </button>
  <div className="collapse navbar-collapse" id="navbarNavDropdown">
    <ul className="navbar-nav mx-auto">
      <li className="nav-item">
        <Link to="/places" className='text-black nav-link'><b>Places</b></Link>
      </li>
     
      <li className="nav-item ml-5">
      <Link to="/cars" className='text-black nav-link'><b>Cars</b></Link>
      </li>
      
           
     </ul>
  </div>
</nav> 
</div>
  )
}

export default NavigationBar