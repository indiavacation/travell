import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

import Navbar from './Components/Navbar';
import Footer from './Components/Footer';
import HomePage from './Pages/HomePage';

import AboutUs from './Pages/AboutUs';
import NavigationBar from './Components/NavgationBar';
import Places from './Pages/Places';
import Cars from './Pages/Cars';
import BookNow from './Pages/BookNow';








function App() {
  return (
    <div>
      <BrowserRouter>
      <Navbar />
      <NavigationBar />
    
      <Routes>
      <Route path='/' element={<HomePage />}  /> 
        <Route path='aboutus' element={<AboutUs />}  /> 
        <Route path='places' element={<Places />} />
        <Route path='cars' element={<Cars />} />
        <Route path='booknow' element={<BookNow />} />
      </Routes>
      </BrowserRouter>
      <Footer />
    </div>
  );
}

export default App;
