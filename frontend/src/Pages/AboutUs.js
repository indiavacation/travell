import React from 'react';
import img5 from '../images/img5.jpg'
function AboutUs () {
  return (
    <div className="about-us-container">
       <img src={img5} style={{border:'1px solid white',padding:'3rem 8rem',width:'80vw',height:'90vh'}} alt=''></img>      
      

      <h2>Welcome to India Travel</h2>
      <p>
        At IndiaTravel, we are passionate about exploring the world and navigating the roads with style and comfort.
        Our mission is to provide you with comprehensive travel and cars guides, helping you make informed decisions and creating unforgettable experiences.
      </p>

      <h3>Our Vision</h3>
      <p>
        We envision a world where every journey is filled with excitement, and every drive is a seamless adventure.
        Through our platform, we strive to be the go-to resource for travel enthusiasts and car aficionados, offering valuable insights and expert advice.
      </p>

      <h3>What Sets Us Apart</h3>
      <ul>
        <li><strong>Expertise:</strong> Our team consists of seasoned travelers and automotive experts who bring a wealth of knowledge to every guide.</li>
        <li><strong>Comprehensive Guides:</strong> From exotic travel destinations to the latest car models, our guides are meticulously curated to provide you with the most up-to-date and relevant information.</li>
        <li><strong>User-Friendly Interface:</strong> We understand the importance of a seamless user experience. Our website is designed to be user-friendly, ensuring that you can easily navigate and find the information you need.</li>
      </ul>

    </div>
  );
};

export default AboutUs;