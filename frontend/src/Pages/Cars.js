import React, { useEffect, useState } from 'react'
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import axios from 'axios';
import { Card,CardBody,CardTitle,CardSubtitle,CardText} from 'reactstrap';
// import carspic from './images/carspic.jpg'
import { useNavigate } from 'react-router-dom';


function Cars() {
    let navigate=useNavigate()
    const handleClick=()=>{
      navigate('/booknow')
    }
 
  let [data, setData] = useState(null)
  useEffect(() => {
    const fetchData = async () => {
      try {
        let response = await axios.get("http://localhost:3001/carfetch");
        console.log(response.data);
        setData(response.data)
      }
      catch (err) {
        console.log(err);
      }

    }
    fetchData();
  }, [])

  
  return (
    <div>
      
      {/* <img src={carspic} style={{border:'1px solid white',width:'100vw',height:'70vh'}} alt=''></img>    */}
<div class='car-container' >
{data && data.map((item)=>(
<Card
style={{
 boxSizing:'border-box',
  margin:'30px',
  height:'40%',
  width:'40%', 
   
}}
> 
<CardBody>
  <CardTitle tag="h5">
    <h1>{item.car}</h1>
  </CardTitle>
  <CardSubtitle
    className="mb-2 text-muted"
    tag="h6"
  >
    {item.package} 
    
  </CardSubtitle>
</CardBody>
<img
  alt="Card cap"
  src={item.imgurl}
  width="100%"
  
 
 
/>
<CardBody>
  <CardText>
   {item.Package}
  </CardText>
  <CardText>
  <h4> Price:{item.Price}</h4>
  </CardText>
  <button onClick={handleClick} className="btn btn-success" >BookNow</button>
</CardBody>

</Card>

))}
</div>



</div>
  )
}

export default Cars