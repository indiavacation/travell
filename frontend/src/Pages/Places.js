import React, { useEffect, useState } from 'react'
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import axios from 'axios';
import { Card,CardBody,CardTitle,CardSubtitle,CardText,Button} from 'reactstrap';
import { useNavigate } from 'react-router-dom';




function Places() {
  let navigate=useNavigate()
  const handleClick=()=>{
    navigate('/booknow')
  }

  const cardStyles ={ 
    margin:'20px 0px',
    justifyContent:'space-between',
    
     
    
  }
 
  let [data, setData] = useState(null)
  useEffect(() => {
    const fetchData = async () => {
      try {
        let response = await axios.get("http://localhost:3001/imagefetch");
        console.log(response.data);
        setData(response.data)
      }
      catch (err) {
        console.log(err);
      }

    }
    fetchData();
  }, [])

  
  return (
    <div>
      

<div style={cardStyles}>
{data && data.map((item)=>(
<Card
style={{
 
 boxSizing:'border-box',
  margin:'50px',
  height:'50%',
  width:'80%',
  

}}
> 
<CardBody>
  <CardTitle tag="h5">
    <h1>{item.productname}</h1>
  </CardTitle>
  <CardSubtitle
    className="mb-2 text-muted"
    tag="h6"
  >
    {item.place} 
    
  </CardSubtitle>
</CardBody>
<img
  alt="Card cap"
  src={item.imgurl}
  width="100%"
  
 
 
/>
<CardBody>
  <CardText>
   {item.Package}
  </CardText>
  <CardText>
  <h4> Price:{item.Price}</h4>
  </CardText>
  <Button onClick={handleClick} type="button" className="btn btn-success">BookNow</Button>
</CardBody>
</Card>

))}
</div>



</div>
  )
}

export default Places