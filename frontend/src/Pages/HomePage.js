import React from 'react'
import CardPart from '../Components/CardPart'
import Homepic from '../images/Homepic.webp'

function HomePage() {
  return (
    <div>
       <img src={Homepic} style={{border:'1px solid white',padding:'3rem 8rem',width:'80vw',height:'90vh'}}></img>
        <CardPart />
    </div>
  )
}

export default HomePage