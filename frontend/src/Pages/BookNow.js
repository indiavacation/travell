import React, { useState } from 'react';
import {Input,FormGroup,Label,Form,Button} from 'reactstrap'
import axios from 'axios'


const BookNow = () => {
  let bookForm={
    border:'1px solid gray', 
    width:'500px',
    padding:'1rem',
    margin:'3rem auto',
    borderRadius:'20px',
    backgroundColor:'skyblue'
  
    }
  

 
  let [formdata,setFormData] = useState({
    FullName:'',
    email:'',
    mobilenumber:'',
    Destination:'',
    departureDate:'',
    returnDate:'',
    check:'false'


  })
  const handleOnchange = (e) =>{
   let {name,value,type,checked} = e.target  
   let inputValue = type === 'checkbox' ? checked:value
     
    setFormData({
     ...formdata,
       [name]:inputValue

     
     
    })
  
  }
  const handleSubmit = (e)=>{
    e.preventDefault();
    // console.log(formdata)
  let BookNow=async()=> {
  try{
    let response=await axios.post("http://localhost:3001/book",formdata)
    console.log(response)
  }
  catch(err) {
    console.log(err)
  
  }
  
}
BookNow()
  }
  
 

  return (
    <div style={bookForm} >
      
      <h2 style={{color:'brown'}}>Package Booking </h2>
      <Form onSubmit={handleSubmit}>
      <FormGroup>
    <Label for="name">
      Full Name:
    </Label>
    <Input
      id="name"
      name="FullName"
      placeholder="name"
    />
  </FormGroup>
  <FormGroup>
        <Label for="exampleEmail">
          Email
        </Label>
        <Input
          id="exampleEmail"
          name="email"
          placeholder="Email"
          type="email"
          value= {formdata.email}
          onChange={handleOnchange}
          
        />
      </FormGroup>
      <FormGroup>
    <Label for="mobile">
      MobileNumber
    </Label>
    <Input
      id="mobile"
      name="mobilenumber"
      placeholder="Number"
      value= {formdata.mobilenumber}
      onChange={handleOnchange}

    />
  </FormGroup>
       

  <FormGroup>
    <Label for="destination">
      Destination:
    </Label>
    <Input
      id="destination"
      name="Destination"
      placeholder="destination"
      value= {formdata.Destination}
      onChange={handleOnchange}

    />
  </FormGroup>

  <FormGroup>
    <Label for="departure">
      Departure Date:
    </Label>
    <Input
      id="departure"
      name="departureDate"
      placeholder=""
      type="date"
      value= {formdata.departureDate}
      onChange={handleOnchange}

    />
  </FormGroup>
  <FormGroup>
    <Label for="return">
      Return Date:
    </Label>
    <Input
      id="return"
      name="returnDate"
      placeholder=""
      type="date"
      value= {formdata.returnDate}
      onChange={handleOnchange}

    />
  </FormGroup>

        <Button type="submit" className='btn btn-danger' onClick={handleSubmit}>Book Now</Button>
      </Form>
    </div>
   
  );
};

export default BookNow;