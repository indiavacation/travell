let express = require('express');
const bodyParser=require('body-parser');
let app = express();
app.use(bodyParser.json())
const cors=require('cors');
app.use(cors());


//GetData
app.get("/",(req,res)=>{
res.send("node js");
})

app.post("/register",(req,res)=>{
    res.send(req.body)
})

app.use('/book',require('./Components/booknowinsert'))
app.use('/booknow',require('./Components/booknow'))
app.use('/home',require('./Components/homefetch'))
app.use('/carfetch',require('./Components/carfetch'))
app.use('/imagefetch',require('./Components/imagefetch'))
app.use('/login',require('./Components/fetch'))
app.use('/fetchdata',require('./Components/fetchdata'))
app.use('/fetch1',require('./Components/fetch1'))
app.use('/fetch1',require('./Components/fetchById'))
app.use('/fetch2',require('./Components/fetch2'))
// app.use('/fetch4',require('./Components/fetch4'))
// app.use('/fetch3',require('./Components/fetch3'))
app.use('/login',require('./Components/login'))
app.listen(3001,()=>{
    console.log("server running")
})